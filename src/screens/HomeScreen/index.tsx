/* eslint-disable no-unused-vars */
import React from 'react';
import { Button, Text, View } from 'react-native';
import styles from './style';
import { NavigationScreenProp, NavigationState, NavigationParams } from 'react-navigation';

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
  name: string;
  enthusiasmLevel?: number;
}

const HomeScreen: React.FC<Props> = (props) => {
  const [enthusiasmLevel, setEnthusiasmLevel] = React.useState(
      props.enthusiasmLevel,
  );

  const onIncrement = () => setEnthusiasmLevel((enthusiasmLevel || 0) + 1);
  const onDecrement = () => setEnthusiasmLevel((enthusiasmLevel || 0) - 1);

  const getExclamationMarks = (numChars: number) => {
    return numChars < 0 ? null : Array(numChars + 1).join('!');
  };

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View style={styles.root}>
        <Text style={styles.greeting}>
          Hello {props.name + getExclamationMarks(enthusiasmLevel || 0)}
        </Text>

        <View style={styles.buttons}>
          <View style={styles.button}>
            <Button
              title="-"
              onPress={onDecrement}
              accessibilityLabel="decrement"
              color="red"
            />
          </View>

          <View style={styles.button}>
            <Button
              title="+"
              onPress={onIncrement}
              accessibilityLabel="increment"
              color="blue"
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default HomeScreen;

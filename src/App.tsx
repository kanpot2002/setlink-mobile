import React from 'react';
import { AppContainer } from './services/tabnavigator/CustomAppContainerService';

export interface Props {

}

const App: React.FC<Props> = (props) => {
  return (
    <AppContainer />
  );
};

export default App;

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from '../../screens/HomeScreen';

const MainApp = createStackNavigator(
    {
      HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
          headerShown: false,
          gestureEnabled: false,
        },
      },
    }, {
      initialRouteName: 'HomeScreen',
      navigationOptions: {
        headerShown: false,
        gestureEnabled: false,
      },
    },
);


const RouteApp = createStackNavigator({
  Main: {
    screen: MainApp,
    navigationOptions: {
      header: undefined,
    },
  },
}, {
  initialRouteName: 'Main',
},
);

export const AppContainer = createAppContainer(RouteApp);
